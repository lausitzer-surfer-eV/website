# <img src="https://lausitzer-surfer.de/images/logo.png" width="96"> Lausitzer Surfer e.V. Webseite

Das Git Repository für die Webseite vom den Lausitzer Surfer e.V.

https://lausitzer-surfer.de

---

1. Um die Webseite zu ändern oder Informationen hinzuzufügen, bitte im Framagit [registrieren](https://framagit.org/users/sign_up) und 
Zugang in der Gruppe [Lausitzer Surfer e.V.](https://framagit.org/lausitzer-surfer-eV) erfragen. 

2. Nach der Freischaltung können Änderungen an der Webseite gemacht werden. Bitte in der [Doku schauen](https://doku.lausitzer-surfer.de/) für eine Anleitung.

3. Nach Änderungen wird die Webseite automatisch erstellt und auf dem Server aktualisiert. Das kann mitunter bis zu 5 Minuten dauern.

---

Die Webseite basiert auf [Hugo](https://gohugo.io/) und dem [Hugo-Scroll Theme](https://zjedi.github.io/hugo-scroll/).

# Entwickler

## Lokal testen

```bash
LAST_COMMIT_DATE=$(git log -1 --format=%cI)
LAST_COMMIT_HASH=$(git rev-parse --short HEAD)
hugo serve
```

## Update keycloak dependency

```bash
npm update
```