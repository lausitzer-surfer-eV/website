---
title: "Ziele"
header_menu_title: "Ziele"
navigation_menu_title: "Ziele"
weight: 2
header_menu: true
---

In den nächsten Jahren wird mit den weiteren eröffneten Seen in der Lausitz viel passieren. Gemeinsam können wir (demokratischen) Einfluss darauf nehmen, wo gute Startbedingungen für Surfer gesichert und geschaffen werden und Strandzugänge und Infrastruktur verbessert werden können.

![Let us get started](images/ziele.jpg)

Unsere drei prioritäten Ziele:
- Zugänge für wichtige Strände und Reviere für alle Surf-Sportarten im Lausitzer Seenland sichern,
erhalten und entwickeln
- Demokratische Interessenvertretung für politische Fragestellungen
- Vernetzung und Nachwuchsförderung

