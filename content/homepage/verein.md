---
title: "Über uns"
weight: 3
header_menu: true
---

![Verein](images/verein.jpg)

##### Gründung

Der Verein wurde am 01. Oktober 2023 gegründet. Am 15. Mai 2024 erhielten wir die Anerkennung
als gemeinnütziger Verein und sind seitdem im Register des Amtsgerichtes Dresden als e.V. 
geführt. 

----

Wenn Ihr beitreten wollt und Fragen habt, schreibt uns unkompliziert an [info@lausitzer-surfer.de](mailto:info@lausitzer-surfer.de). Anschließend könnt Ihr einen Aufnahmeantrag mit dem Formular [unten](#kontakt) stellen. Oder noch besser: Ihr nutzt unsere Online-Registrierung unter [mitglieder.lausitzer-surfer.de](https://mitglieder.lausitzer-surfer.de). Unsere Beiträge:

| Beitrags-klasse | Mitgliedsform                       | Beitragshöhe pro Jahr in EUR |
| --------------- | ----------------------------------- | ---------------------------- |
| 01              | Kinder und Jugendliche bis 18 Jahre | o.B.                         |
| 02              | Erwachsene über 18 Jahre            | € 10,00                      |
| 03              | Ehrenmitglieder                     | o.B.                         |
| 04              | fördernde Mitglieder                | ab € 10,00                   |

Falls Ihr spenden möchtet, nutzt bitte folgende Kontoverbindung:

> Lausitzer Surfer e.V, Verwendungszweck: Spende  
> - IBAN: DE618306 5408 0005 4370 59  
> - BIC: GENODEFISLR (Skatbank)  

Wir stellen Euch anschließend gern eine Spendenbescheinigung aus.