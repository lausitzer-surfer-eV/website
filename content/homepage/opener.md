---
title: "F&uuml;r alle Surfer"
weight: 1
---

**Windsurfen, Surffoilen, Wingfoilen, Kitesurfen, Stand Up Paddling (SUP), SUP-Foilen und
Surfen**

Die Lausitz ist ein Surfrevier mit großem Potential, jedoch mit derzeit nur gering vernetzten Surfern. Hier wollen wir mit dem Lausitzer Surfer e.V. ein Netzwerk für alle Surfsportarten schaffen. Windsurfer, Foilsurfer, Wingfoiler, SUP, Surffoiler und Kitesurfer sind bei uns alle gleichberechtigt willkommen. Genauso laden wir Surfer aus den angrenzenden Ländern Polen und Tschechien herzlich ein, uns beizutreten.