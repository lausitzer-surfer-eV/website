---
title: "Mitglieder"
weight: 5
header_menu: true
---

Vereinssitz: Dresden

Vereinsvorstand und Gründungsmitglieder

- {{<extlink text="Vorsitzender Dr. Alexander Dunkel">}}
- {{<extlink text="Stellvertretender Vorsitzender Kristof Ahnert">}}
- {{<extlink text="Schatzmeister und Vorstandsmitglied Dr. Steve Kupke">}}
- {{<extlink text="Datenschutzbeauftragter und Vorstandsmitglied Thomas Forner">}}
- {{<extlink text="Gründungsmitglied Dr. André Heinzig">}}
- {{<extlink text="Gründungsmitglied Isabel Reimelt">}}
- {{<extlink text="Gründungsmitglied Philipp Wagner">}}

Der Verein vertritt derzeit 31 Mitglieder. [^1]
[^1]: Stand 08. Dezember 2024



