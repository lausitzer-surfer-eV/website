---
title: "Vernetzung"
weight: 6
header_menu: true
---

{{<icon class="fa fa-external-link">}}&nbsp;[info.lausitzer-surfer.de](https://info.lausitzer-surfer.de/)

Hier findet Ihr Informationen zu den Surf Spots in der Lausitz 
und zur Organisation vom Lausitzer Surfer e.V. und einen [Blog über Neues vom Verein](https://info.lausitzer-surfer.de/neues/).