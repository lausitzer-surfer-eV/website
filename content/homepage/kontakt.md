---
title: "Kontakt"
weight: 4
header_menu: true
---

{{<icon class="fa fa-envelope">}}&nbsp;[info@lausitzer-surfer.de](mailto:info@lausitzer-surfer.de)

🤙 Hang Loose!

{{<icon class="fa fa-file">}}&nbsp;[Satzung](/pdf/01_Satzung.pdf) | {{<icon class="fa fa-file">}}&nbsp;[Beitragsordnung](pdf/02_Beitragsordnung.pdf) | {{<icon class="fa fa-file">}}&nbsp;[Datenschutzerklärung](pdf/04_Datenschutzerkl%C3%A4rung_Mitglieder.pdf)

{{<icon class="fa fa-address-card">}}&nbsp;{{< rawhtml >}}<a id="registerURL" href="https://mitglieder.lausitzer-surfer.de">Online-Mitgliedsregistrierung</a>{{< /rawhtml >}} | [Account Selbstverwaltung](https://mitglieder.lausitzer-surfer.de/realms/Mitglieder/account/#/)

{{<icon class="fa fa-file">}}&nbsp;[Offline-Aufnahmeantrag](pdf/03_Aufnahmeantrag.pdf)


