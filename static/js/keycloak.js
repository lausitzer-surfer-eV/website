// Keycloak Register Link Script

import Keycloak from '/js/keycloak-js/lib/keycloak.js';

const keycloak = new Keycloak({
    "realm": "Mitglieder",
    "clientId": "account-console",
    "auth-server-url": "https://mitglieder.lausitzer-surfer.de/",
    "url": "https://mitglieder.lausitzer-surfer.de/",
    "ssl-required": "external",
    "resource": "account-console",
    "public-client": true,
    "confidential-port": 0,
    "enableLogging": false
});

try {
    const authenticated = await keycloak.init({ pkceMethod: "S256" }).then().catch(function () {
        // alert('failed to initialize');
        // return default URL on error
        document.getElementById('registerURL').href = "https://mitglieder.lausitzer-surfer.de/";
    });
    if (authenticated) {
        console.log('User is authenticated');
    } else {
        console.log('User is not authenticated');
    }
    // returns promise
    const registerUrl = keycloak.createRegisterUrl()
    // turn promise into URL
    const printAddress = () => {
        registerUrl.then((a) => {
            document.getElementById('registerURL').href = a;
        });
      };
    printAddress();

} catch (error) {
    console.error('Failed to initialize adapter:', error);
}