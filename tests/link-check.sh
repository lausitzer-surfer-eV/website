#!/bin/bash

LOCAL_HOST="http://localhost:1313/"
MAX_WAIT_TIME=60 # 30 sec
# todo: replace http://localhost:1313/pdf/ with production
OPTIONS="--exclude 'reddit.com' \
         --exclude 'docker.com' \
         --exclude '^*.webm$' \
         --exclude 'http://localhost:1313/impressum.html' \
         --exclude 'http://localhost:1313/pdf/*' \
         --exclude 'https://github.com/' \
         --ignore-fragments \
         --max-response-body-size 100000000 \
         --junit > rspec.xml"

for i in $(seq 0 ${MAX_WAIT_TIME}); do # 5 min
    sleep 0.5
    IS_SERVER_RUNNING=$(curl -LI ${LOCAL_HOST} -o /dev/null -w '%{http_code}' -s)
    if [[ "${IS_SERVER_RUNNING}" == "200" ]]; then
        eval muffet "${OPTIONS}" ${LOCAL_HOST} && exit 0 || exit 1
    fi
done

echo "error: time out $((${MAX_WAIT_TIME}/2)) sec" && exit 1
